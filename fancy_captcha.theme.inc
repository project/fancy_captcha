<?php

/**
 * @file
 * Fancy Captcha theme implementations.
 */

/**
 * Default theme implementation for fancy_captcha.
 */
function theme_fancy_captcha(&$variables) {
  $element = $variables['element'];

  // Generate the markup needed for the widget.
  $output = '<div class="fancy-captcha">';
  $items = array();
  foreach ($element['#fc_options'] as $option) {
    $items[] = array(
      'data' => $option,
      'class' => array("fancy-captcha-$option"),
    );
  }
  $output .= theme('item_list', array('items' => $items));
  $output .= '<input type="text" class="fancy-captcha-solution" name="' . $element['#name'] . '" />';
  $output .= '</div>';

  // Add the CSS asset and inline CSS for our items.
  fancy_captcha_add_css($element['#items']);

  return $output;
}

/**
 * Adds CSS to show icons for fancy captcha widget.
 */
function fancy_captcha_add_css($items) {

  $css = '';
  $display = array();

  // Set appropriate background.
  foreach ($items as $key => $item) {
    switch ($item) {
      case 'pencil':
        $css .= ".item-list ul li.fancy-captcha-$key { background-position: 0px -184px; }\n";
        break;
      case 'book':
        $css .= ".item-list ul li.fancy-captcha-$key { background-position: 0px -92px; }\n";
        break;
      case 'clock':
        $css .= ".item-list ul li.fancy-captcha-$key { background-position: 0px 0px; }\n";
        break;
      case 'scissors':
        $css .= ".item-list ul li.fancy-captcha-$key { background-position: 0px -230px; }\n";
        break;
      case 'note':
        $css .= ".item-list ul li.fancy-captcha-$key { background-position: 0px -138px; }\n";
        break;
      case 'heart':
        $css .= ".item-list ul li.fancy-captcha-$key { background-position: 0px -46px; }\n";
        break;
    }
    $display[] = ".item-list ul li.fancy-captcha-$key";
  }

  // Display apprpriate items.
  $css .= implode(', ', $display) . ' { display: block; }';

  $options = array(
    'type' => 'inline',
    'group' => CSS_THEME + 1,
  );
  drupal_add_css($css, $options);
}
