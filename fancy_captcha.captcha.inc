<?php

/**
 * @file
 * Captcha functionality for the Fancy Captcha module.
 *
 * This code is located in a seperate file, and is only utilized when needed.
 */
function _fancy_captcha_captcha_generate() {
  $result = array();
  $items = fancy_captcha_items();
  list($result['solution'], $item) = fancy_captcha_solution($items);
  $options = fancy_captcha_options($items);
  $result['form']['captcha_response'] = array(
    '#type' => 'fancy_captcha',
    '#title' => t('Enter the code for :item', array(':item' => $item)),
    '#fc_options' => $options,
    '#items' => $items,
    '#required' => TRUE,
  );
  return $result;
}
