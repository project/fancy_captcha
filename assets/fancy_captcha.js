/**
 * @file
 * Fancy Captcha jQuery plugin.
 */
(function ($) {
    $.fn.fancyCaptcha = function() {

        this.addClass('fancy-captcha-processed');
        label = this.siblings('label').hide();
        thing = label.find('em').text();
        this.prepend('<div class="fancy-captcha-prompt">Drag the <span class="fancy-captcha-answer">' + thing + '</span> into the circle.</div>');
        this.prepend('<div class="fancy-captcha-circle"></div>');

        this.find('li').draggable({
            containment: ".fancy-captcha"
        });

        // Identify the field where the result should go.
        field = this.find("input");

        // When items ar dropped into the circle, their value is set on the text field.
        this.find(".fancy-captcha-circle").droppable({
            drop: function(event, ui) {
                field.val(ui.draggable.text());
            }
        });

        return this;
    };
    
})(jQuery);
